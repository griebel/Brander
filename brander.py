#!/usr/bin/python

# import the necessary packages
from imutils import paths
from imutils import resize as rs
import numpy as np
import argparse
import cv2
import os

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-w", "--watermark", required=True,
	help="path to watermark image (assumed to be transparent PNG)")
ap.add_argument("-i", "--input", required=True,
	help="path to the input directory of images")
ap.add_argument("-o", "--output", required=True,
	help="path to the output directory")
ap.add_argument("-a", "--alpha", type=float, default=0.25,
	help="alpha transparency of the overlay (smaller is more transparent)")
ap.add_argument("-c", "--correct", type=int, default=1,
	help="flag used to handle if bug is displayed or not")
ap.add_argument("-s", "--scale", type=float, default=1,
	help="scaling factor of the logo image to shrink or increase the size")
args = vars(ap.parse_args())

# load the watermark image, making sure we retain the 4th channel
# which contains the alpha transparency
watermark = cv2.imread(args["watermark"], cv2.IMREAD_UNCHANGED)
(wH, wW) = watermark.shape[:2]

# resize image to maximal 1024 px
if wW > wH:
    watermark = rs(watermark, width=200)
else:
    watermark = rs(watermark, height=200)

# scale it user defined
watermark = cv2.resize(watermark, (0,0), fx=args["scale"], fy=args["scale"])
(wH, wW) = watermark.shape[:2]


# split the watermark into its respective Blue, Green, Red, and
# Alpha channels; then take the bitwise AND between all channels
# and the Alpha channels to construct the actaul watermark
# NOTE: I'm not sure why we have to do this, but if we don't,
# pixels are marked as opaque when they shouldn't be
if args["correct"] > 0:
	(B, G, R, A) = cv2.split(watermark)
	B = cv2.bitwise_and(B, B, mask=A)
	G = cv2.bitwise_and(G, G, mask=A)
	R = cv2.bitwise_and(R, R, mask=A)
	watermark = cv2.merge([B, G, R, A])

# loop over the input images
for imagePath in paths.list_images(args["input"]):
	# load the input image
	image = cv2.imread(imagePath)

        # resize image to maximal 1024 px
	(h, w) = image.shape[:2]
        if w > h:
            image = rs(image, width=1024)
        else:
            image = rs(image, height=1024)

	# add an extra dimension to the
	# image (i.e., the alpha transparency)
	(h, w) = image.shape[:2]
	image = np.dstack([image, np.ones((h, w), dtype="uint8") * 255])

	# construct an overlay that is the same size as the input
	# image, (using an extra dimension for the alpha transparency),
	# then add the watermark to the overlay in the bottom-right
	# corner
	overlay = np.zeros((h, w, 4), dtype="uint8")
	overlay[h - wH - 20:h - 20, w - wW - 20:w - 20] = watermark

	# blend the two images together using transparent overlays
	output = image.copy()
        cv2.addWeighted(overlay, args["alpha"], output, 1.0, 0, output)

	# write the output image to disk
	filename = imagePath[imagePath.rfind(os.path.sep) + 1:]
	p = os.path.sep.join((args["output"], filename))
	cv2.imwrite(p, output)
